angular.module('kasirApp', ['ui.router', 'ui.bootstrap', 'ngTable'])
  .constant('BASE_URL', 'http://localhost/skill-test-jois')
  .config(function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/');
    $stateProvider
      .state('index', {
        url: '/',
        templateUrl: 'src/templates/_main.html',
      });
  })
  .controller('MasterCtrl', function ($scope, $http, $state, BASE_URL) {
    if (!localStorage.getItem('isLoggedIn')) {
      window.open("./login.html", "_self");
    }
    $scope.user = {};
    $scope.pass = {};
    if (localStorage.getItem('user')) {
      $scope.user = JSON.parse(localStorage.getItem('user'));
    }

    $scope.onLogout = function () {
      localStorage.removeItem('isLoggedIn');
      localStorage.removeItem('user');
      window.open("./login.html", "_self");
    };
  })
  .controller('LoginCtrl', function ($scope, $http, BASE_URL) {
    $scope.user = {};
    $scope.logging = false;

    $scope.onLogin = function ($event) {
      $event.preventDefault();
      $scope.logging = true;
      if (!$scope.user.username && !$scope.user.password) {
        $scope.logging = false;
        alert('Lengkapi Form Pengisian!');
        return;
      }
      $http.post(BASE_URL + '/server/api/user/login.php', $scope.user).then(function (response) {
        console.log('response', response);
        if (response.data.success) {
          $scope.logging = false;
          localStorage.setItem('isLoggedIn', true);
          localStorage.setItem('user', JSON.stringify(response.data.user));
          window.open("./", "_self");
        } else {
          $scope.logging = false;
          alert(response.data.msg);
          return;
        }
      }).catch(function (err) {
        $scope.logging = false;
        alert('Maaf telah terjadi error saat menghubungi ke server! Gagal Login');
        console.log('err', err);
      });
    };
  })
  .controller('DropdownController', DropdownController)


function DropdownController() {
  var vm = this;
  vm.isCollapsed = true;
  vm.status = {
    isopen: false
  }
}