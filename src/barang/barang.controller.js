angular.module('kasirApp')
  .controller('BarangCtrl', function ($scope, $http, NgTableParams, $state, BASE_URL) {

    $scope.table = new NgTableParams({
      count: 10,
      page: 1
    }, { getData: getData });

    function getData(params) {
      var name = params.filter().name ? params.filter().name : '';
      return $http.get(BASE_URL + '/server/api/barang/data.php?page=' + params.page() + '&limit=' + params.count() + '&nama_barang=' + name).then(function (response) {
        console.log('res', response);
        params.total(response.data.total);
        return response.data.results;
      }).catch(function (err) {
        if (err) console.log(err);
      });
    }

    $scope.onDelete = function (id, barang) {
      if (confirm('Apakah Kamu Yakin Ingin Menghapus Data Ini ? ' + barang.nama_barang)) {
        $http.get(BASE_URL + '/server/api/barang/delete.php?kode_barang=' + id).then(function (response) {
          alert('Berhasil Menghapus Data!');
          $state.reload();
        }).catch(function (err) {
          console.log(err);
          alert('Oopss!! Ada Kesalahan Saat Menghapus Data Dari Server!');
        });
      }
    };

  });