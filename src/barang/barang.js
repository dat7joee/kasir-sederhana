angular.module('kasirApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('barang', {
        abstract: true,
        templateUrl: 'src/templates/_layout.html'
      })
      .state('barang.index', {
        url: '/barangs',
        templateUrl: 'src/barang/barang.html',
        controller: 'BarangCtrl'
      })
      .state('barang.new', {
        url: '/barangs/new',
        templateUrl: 'src/barang/form/barang.html',
        controller: 'BarangFormCtrl'
      })
      .state('barang.edit', {
        url: '/barangs/edit/:id',
        templateUrl: 'src/barang/form/barang.html',
        controller: 'BarangFormCtrl'
      });
  });