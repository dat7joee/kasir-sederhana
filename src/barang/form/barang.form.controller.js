angular.module('kasirApp')
  .controller('BarangFormCtrl', function ($scope, $http, $state, $stateParams, BASE_URL) {
    $scope.model = {};
    $scope.onEdit = false;
    if ($stateParams.id) {
      $scope.onEdit = true;
      $http.get(BASE_URL + '/server/api/barang/show.php?kode_barang=' + $stateParams.id).then(function (response) {
        if (response.data.success) {
          $scope.model = response.data.barang;
        } else {
          alert('Oopss..! Data Tidak Ditemukan');
          $state.go('^.index');
        }
      }).catch(function (err) {
        $state.go('^.index');
        console.log('err', err);
        alert('Oopss!! Data Tidak Ditemukan! ' + err.data.msg ? err.data.msg : err.data);
      });
    }

    $scope.onSave = function () {
      if (confirm('Apakah Kamu Yakin Ingin Menyimpan Data Ini ?')) {
        if (!$stateParams.id) {
          $http.post(BASE_URL + '/server/api/barang/save.php', $scope.model).then(function (response) {
            alert('Berhasil Menyimpan!');
            $state.go('^.index');
          }).catch(function (err) {
            console.log(err);
            alert('Oopss! Error Terjadi Ketika Menghubungi Server!');
          });
        } else {
          $http.post(BASE_URL + '/server/api/barang/update.php?kode_barang=' + $stateParams.id, $scope.model).then(function (response) {
            alert('Berhasil Menyimpan!');
            $state.go('^.index');
          }).catch(function (err) {
            console.log(err);
            alert('Oopss! Error Terjadi Ketika Menghubungi Server!');
          });
        }
      }
    };
  });