angular.module('kasirApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('penjualan', {
        abstract: true,
        templateUrl: 'src/templates/_layout.html'
      })
      .state('penjualan.index', {
        url: '/penjualans',
        templateUrl: 'src/penjualan/penjualan.html',
        controller: 'PenjualanCtrl'
      })
      .state('penjualan.new', {
        url: '/penjualans/new',
        templateUrl: 'src/penjualan/form/penjualan.html',
        controller: 'PenjualanFormCtrl'
      })
      .state('penjualan.show', {
        url: '/penjualans/show/:id',
        templateUrl: 'src/penjualan/show.html',
        controller: 'PenjualanShowCtrl'
      });
  });