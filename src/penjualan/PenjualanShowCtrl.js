angular.module('kasirApp')
  .controller('PenjualanShowCtrl', function ($scope, $http, $state, $stateParams, BASE_URL) {
    $scope.model = {};
    if ($stateParams.id) {
      $http.get(BASE_URL + '/server/api/penjualan/show.php?id_penjualan=' + $stateParams.id).then(function (response) {
        console.log('data', response);
        if (response.data.success) {
          $scope.model = response.data;
        } else {
          alert('Oopss..! Data Tidak Ditemukan');
          // $state.go('^.index');
        }
      }).catch(function (err) {
        $state.go('^.index');
        console.log('err', err);
        alert('Oopss!! Data Tidak Ditemukan! ' + err.data.msg ? err.data.msg : err.data);
      });
    } else {
      $state.go('^.index');
    }

  });