angular.module('kasirApp')
  .controller('PenjualanFormCtrl', function ($scope, $http, $state, $stateParams, BASE_URL) {
    $scope.model = {};
    $scope.model.total_penjualan = 0;
    $scope.model.details = [];
    $scope.onEdit = false;

    $scope.onSave = function () {
      if (confirm('Apakah Kamu Yakin Ingin Menyimpan Data Ini ?')) {
        if (!$stateParams.id) {
          $http.post(BASE_URL + '/server/api/penjualan/save.php', $scope.model).then(function (response) {
            alert('Berhasil Menyimpan!');
            $state.go('^.index');
          }).catch(function (err) {
            console.log(err);
            alert('Oopss! Error Terjadi Ketika Menghubungi Server!');
          });
        }
        // else {
        //   $http.post(BASE_URL + '/server/api/penjualan/update.php?kode_penjualan=' + $stateParams.id, $scope.model).then(function (response) {
        //     alert('Berhasil Menyimpan!');
        //     $state.go('^.index');
        //   }).catch(function (err) {
        //     console.log(err);
        //     alert('Oopss! Error Terjadi Ketika Menghubungi Server!');
        //   });
        // }
      }
    };

    $scope.onAdd = function () {
      if (!$scope.holder.barang && !$scope.holder.qty) {
        return;
      }
      console.log('holder', $scope.holder);
      var harga_barang = Number($scope.holder.barang.harga_barang);
      if ($scope.model.details.length > 0) {
        var onIndex = $scope.model.details.findIndex(function (detail) {
          return detail.kode_barang === $scope.holder.barang.kode_barang
        });

        if (onIndex > -1) {
          $scope.model.details[onIndex].qty += $scope.holder.qty;
          var newTotal = karatSuba($scope.model.details[onIndex].harga_barang, $scope.model.details[onIndex].qty);
          $scope.model.details[onIndex].total_harga = newTotal;
        } else {
          $scope.model.details.push({
            kode_barang: $scope.holder.barang.kode_barang,
            nama_barang: $scope.holder.barang.nama_barang,
            qty: $scope.holder.qty,
            harga_barang: harga_barang,
            total_harga: karatSuba(harga_barang, $scope.holder.qty)
          });
        }
      } else {
        $scope.model.details.push({
          kode_barang: $scope.holder.barang.kode_barang,
          nama_barang: $scope.holder.barang.nama_barang,
          qty: $scope.holder.qty,
          harga_barang: harga_barang,
          total_harga: karatSuba(harga_barang, $scope.holder.qty)
        });
      }
      $scope.holder.barang = null;
      $scope.holder.qty = 0;
      $scope.hitungTotal();
    };

    $scope.onRemove = function (index) {
      $scope.model.details.splice(index, 1);
      $scope.hitungTotal();
    }

    $scope.hitungTotal = function () {
      var total = 0;
      $scope.model.details.map(function (detail) {
        total += detail.total_harga;
      });

      $scope.model.total_penjualan = total;
    }

    $scope.getBarang = function (value) {
      return $http.get(BASE_URL + '/server/api/barang/search.php?nama_barang=' + value).then(function (response) {
        console.log('respo', response);
        return response.data;
      }).catch(function (err) {
        console.log(err);
      });
    }

    function karatSuba(x, y) {
      var x1, x0, y1, y0, base, m;
      base = 10;


      if ((x < base) || (y < base)) {
        console.log(" X - y = ", x, y, x * y)
        return x * y;
      }
      var dummy_x = x.toString();
      var dummy_y = y.toString();

      var n = (dummy_x.length > dummy_y.length) ? dummy_y.length : dummy_x.length;
      m = Math.round(n / 2);

      var high1 = parseInt(dummy_x.substring(0, dummy_x.length - m));
      var low1 = parseInt(dummy_x.substring(dummy_x.length - m, dummy_x.length));

      var high2 = parseInt(dummy_y.substring(0, dummy_y.length - m));
      var low2 = parseInt(dummy_y.substring(dummy_y.length - m, dummy_y.length));

      var z0 = karatSuba(low1, low2);
      var z1 = karatSuba(low1 + high1, low2 + high2);
      var z2 = karatSuba(high1, high2);

      var res = (z2 * Math.pow(10, 2 * m)) + ((z1 - z2 - z0) * Math.pow(10, m)) + z0;
      return res;
    }
  });