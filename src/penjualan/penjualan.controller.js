angular.module('kasirApp')
  .controller('PenjualanCtrl', function ($scope, $http, NgTableParams, $state, BASE_URL) {

    $scope.table = new NgTableParams({
      count: 10,
      page: 1
    }, { getData: getData });

    function getData(params) {
      return $http.get(BASE_URL + '/server/api/penjualan/data.php?page=' + params.page() + '&limit=' + params.count()).then(function (response) {
        console.log('res', response);
        params.total(response.data.total);
        return response.data.results;
      }).catch(function (err) {
        if (err) console.log(err);
      });
    }

    $scope.onDelete = function (id, penjualan) {
      if (confirm('Apakah Kamu Yakin Ingin Menghapus Data Ini ? ' + penjualan.id_penjualan)) {
        $http.get(BASE_URL + '/server/api/penjualan/delete.php?kode_penjualan=' + id).then(function (response) {
          alert('Berhasil Menghapus Data!');
          $state.reload();
        }).catch(function (err) {
          console.log(err);
          alert('Oopss!! Ada Kesalahan Saat Menghapus Data Dari Server!');
        });
      }
    };

    $scope.getDataHarian = function () {
      window.open(BASE_URL + '/server/api/penjualan/report.php', '_blank');
    };

  });