<?php
require_once('../../koneksi.php');
if(!$_GET['kode_barang']) {
  http_response_code(500);
  echo json_encode(array('success' => false, 'msg' => 'kode_barang KOSONG!'));
  return;
}

$kode_barang = $_GET['kode_barang'];
$sql = "SELECT * FROM data_barang WHERE kode_barang = '$kode_barang'";
$run = mysqli_query($conn, $sql);
$result = mysqli_fetch_assoc($run);

if($run) {
  if($result['kode_barang']) {
    $result['harga_barang'] = (int) $result['harga_barang'];
    http_response_code(200);
    echo json_encode(array('success' => true, 'barang' => $result));
  } else {
    http_response_code(200);
    echo json_encode(array('success' => false));
  }
} else {
  http_response_code(500);
  echo json_encode(array('success' => false, 'err' => mysql_error()));
}

?>