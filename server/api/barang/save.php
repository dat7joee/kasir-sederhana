<?php
require_once('../../koneksi.php');

$data = json_decode(file_get_contents('php://input'), true);

$nama_barang = $data['nama_barang'];
$harga_barang = $data['harga_barang'];

$kode_barang = null;

$sqlLastId = mysqli_query($conn, "SELECT MAX(kode_barang) FROM data_barang");
$lastId = mysqli_fetch_array($sqlLastId);

if($lastId) {
  $nilai = substr($lastId[0], 2);
  $kode = (int) $nilai;

  $kode = $kode + 1;
  $kode_barang = "A-".str_pad($kode, 3, "0", STR_PAD_LEFT);
} else {
  $kode_barang = 'A-001';
}

$sql = "INSERT INTO data_barang VALUES('$kode_barang', '$nama_barang', '$harga_barang')";
$run = mysqli_query($conn, $sql);

if($run) {
  http_response_code(200);
  echo json_encode(array('success' => true));
} else {
  http_response_code(500);
  echo json_encode(array('success' => false, 'err' => mysql_error()));
}

?>