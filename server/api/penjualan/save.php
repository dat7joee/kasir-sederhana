<?php
require_once('../../koneksi.php');

$data = json_decode(file_get_contents('php://input'), true);

// for data_penjualan
$total_penjualan = $data['total_penjualan'];
$id_penjualan = null;
$sqlLastId = mysqli_query($conn, "SELECT MAX(id_penjualan) FROM data_penjualan");
$lastId = mysqli_fetch_array($sqlLastId);
if($lastId) {
  $nilai = substr($lastId[0], 2);
  $kode = (int) $nilai;

  $kode = $kode + 1;
  $id_penjualan = "P-".str_pad($kode, 3, "0", STR_PAD_LEFT);
} else {
  $id_penjualan = 'P-001';
}

$sql = "INSERT INTO data_penjualan VALUES('$id_penjualan', '$total_penjualan', NOW())";
$run = mysqli_query($conn, $sql);

// for detail penjualan
$details = $data['details'];
foreach($details as $dt) {
  $kode_barang = $dt['kode_barang'];
  $qty = $dt['qty'];
  $harga_barang = $dt['harga_barang'];
  $total = $dt['total_harga'];
  
  $sqlDetail = "INSERT INTO data_detail_penjualan VALUES('$id_penjualan', '$kode_barang', '$qty', '$harga_barang', '$total')";
  mysqli_query($conn, $sqlDetail);
}

if($run) {
  http_response_code(200);
  echo json_encode(array('success' => true));
} else {
  http_response_code(500);
  echo json_encode(array('success' => false, 'err' => mysql_error()));
}

?>