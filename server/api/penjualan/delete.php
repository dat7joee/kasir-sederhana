<?php
require_once('../../koneksi.php');
if(!$_GET['id_penjualan']) {
  http_response_code(500);
  echo json_encode(array('success' => false, 'msg' => 'id_penjualan KOSONG!'));
  return;
}

$id_penjualan = $_GET['id_penjualan'];
$sql = "DELETE FROM data_penjualan WHERE id_penjualan = '$id_penjualan'";
$sqlDetail = "DELETE FROM data_detail_penjualan WHERE id_penjualan = '$id_penjualan'";
$run = mysqli_query($conn, $sql);
$runDetail = mysqli_query($conn, $sqlDetail);

if($run && $runDetail) {
  http_response_code(200);
  echo json_encode(array('success' => true));
} else {
  http_response_code(500);
  echo json_encode(array('success' => false, 'err' => mysql_error()));
}

?>