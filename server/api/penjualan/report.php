<?php
require_once('../../koneksi.php');
require_once('../../mpdf/mpdf.php');

$sql = "SELECT * FROM data_penjualan WHERE DATE(created) = CURDATE()";

$run = mysqli_query($conn, $sql);
$result = null;
if($run) {
  while($row = mysqli_fetch_assoc($run)) {
    $result .= "
      <tr style='border: 1px solid'>
        <td style='border: 1px solid'>".$row['id_penjualan']."</td>
        <td style='border: 1px solid'>Rp ".number_format($row['total_penjualan'], 2)."</td>
        <td style='border: 1px solid'>".date('d/m/y h:m:s', strtotime($row['created']))."</td>
      </tr>
    ";
  }
} else {
  http_response_code(500);
  echo json_encode(array('success' => false, 'err' => mysql_error()));
}

$mpdf = new mPDF('utf-8', 'A4', 10.5, 'arial');
ob_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <!-- <link rel="stylesheet" href="http://localhost/atk/components/css/bootstrap.min.css">
  <link rel="stylesheet" href="http://localhost/atk/components/css/font-awesome.min.css"> -->
  <title>Laporan Penjualan</title>
  <style>
    .header img {
      margin-top: -20px;
      margin-left: 10px;
      float: left;
    }

    .header-text {
      margin-top: 25px;
      text-align: center;
      clear: right;
    }

    .tgl {
      float: right;
      margin-top: 5px;
    }

    .content {
      clear: both;
    }

    table {
      width: 100%;
      margin-left: 0%;
    }

    table td {
      text-align: center;
      padding: 10px 5px;
    }
  </style>
</head>
<body>
  <div class="header">
    <img src="../../assets/logo.png" alt="LOGO" width="60px" height="60px">
  </div>
  <div class="header-text">
    <h2>LAPORAN PENJUALAN</h2>
  </div>
  <!-- <hr> -->
  <hr>
  <div class="content">
    <p>Perihal: Laporan Penjualan <?php echo date('d/m/Y'); ?></p>
    <br>
    <br>
    <table style="border: 1px solid">
      <thead>
        <tr style="border: 1px solid">
          <th style="border: 1px solid">ID Penjualan</th>
          <th style="border: 1px solid">Total Penjualan</th>
          <th style="border: 1px solid">Dibuat</th>
        </tr>
      </thead>
      <tbody>
      <?php
        echo $result;
      ?>
      </tbody>
    </table>
  </div>
  <script>
  </script>
</body>
</html>
<?php
$html = ob_get_contents();
ob_end_clean();
$mpdf->WriteHTML(utf8_encode($html));
$mpdf->Output('laporan_harian_penjualan.pdf', 'I');
?>