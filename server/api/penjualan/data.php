<?php
require_once('../../koneksi.php');

// $npm = $_GET['npm'];
$page = 1;
$limit = 10;

if(isset($_GET['page'])) {
	$page = $_GET['page'];
}

if(isset($_GET['limit'])) {
	$limit = $_GET['limit'];
}

$skip = ($page - 1) * $limit;

$sql = "SELECT * FROM data_penjualan ORDER BY DATE(created) DESC LIMIT $limit OFFSET $skip";
$result = mysqli_query($conn, $sql);

$sqlTotal = "SELECT COUNT(id_penjualan) as totalRow FROM data_penjualan";
$resTotal = mysqli_query($conn, $sqlTotal);

$rowcount = mysqli_fetch_assoc($resTotal);

$output = array();

while($row = mysqli_fetch_assoc($result)) {
	$output[] = $row;
}

echo json_encode(array('results' => $output, 'total' => intval($rowcount['totalRow'])));
?>