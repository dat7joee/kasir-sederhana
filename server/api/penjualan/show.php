<?php
require_once('../../koneksi.php');
if(!$_GET['id_penjualan']) {
  http_response_code(500);
  echo json_encode(array('success' => false, 'msg' => 'id_penjualan KOSONG!'));
  return;
}

$id_penjualan = $_GET['id_penjualan'];
$sql = "SELECT * FROM data_penjualan WHERE id_penjualan = '$id_penjualan'";
$run = mysqli_query($conn, $sql);
$result = mysqli_fetch_assoc($run);

if($run) {
  if($result['id_penjualan']) {
    $sqlDetail = "SELECT * FROM data_detail_penjualan LEFT JOIN data_barang ON data_detail_penjualan.kode_barang = data_barang.kode_barang WHERE data_detail_penjualan.id_penjualan = '$id_penjualan'";
    $runDetail = mysqli_query($conn, $sqlDetail);
    $resultDetail = array();
    echo mysqli_error($conn);
    while($row = mysqli_fetch_assoc($runDetail)) {
      $resultDetail[] = $row;
    }

    http_response_code(200);
    echo json_encode(array('success' => true, 'barang' => $result, 'details' => $resultDetail));
  } else {
    http_response_code(200);
    echo json_encode(array('success' => false));
  }
} else {
  http_response_code(500);
  echo json_encode(array('success' => false, 'err' => mysql_error()));
}

?>